// Airport.cpp

#include "Airport.hpp" // includes airport header file to airport c++ file
#include <queue> // includes queue library to airport C++ file
#include <cstdlib> // includes c standard library to airport C++ file
#include <iostream> // includes iostream library to airport c++ file

using namespace std; // namespace standard allows code to flow without the hassle of :: whenever a function is called

void Airport( // void function airport, function will return the given value if true
             int landingTime, // Time segments needed for one plane to land
             int takeoffTime, // Time segs. needed for one plane to take off
             double arrivalProb, // Probability that a plane will arrive in
             // any given segment to the landing queue
             double takeoffProb, // Probability that a plane will arrive in
             // any given segment to the takeoff queue
             int maxTime, // Maximum number of time segments that a plane
             // can stay in the landing queue
             int simulationLength// Total number of time segs. to simulate
){
    queue<int> takeoffQ, landingQ; // integer queue sets takeoffQ and landingQ to be used later in the code
    int landingWaits = 0, takeoffWaits = 0, timeFree = 0, crashes = 0; // integers landingWaits, takeoffWaits, timeFree
    // and crashes all set to zero in order to free up space to manipulate them later
    int numLandings = 0, numTakeoffs = 0; // integers numLandings and numTakeoffs set to zero to manipulate them later
    
    srand(time(NULL)); // sets the random number generator for time to NULL
    
    for (int curTime = 0; curTime < simulationLength; curTime++) { // for loops, int curTime set to 0 and curTim is now
        //less than simulationLength, curTime increments by 1, curTime will run untilit is greater than or equal to simulationLength
        
        if (((double) rand() / (double) RAND_MAX) < arrivalProb) { // nested if statement double integer rand() divided by double integer RAND_MAX is less than arrivalProb, the inner code will run the incremented amount of times in the for loop
            
            landingQ.push(curTime); // will run if the if statement and the for loop's requirements are met
            
        } // ends the if statements code, for still running
        if (((double) rand() / (double) RAND_MAX) < takeoffProb) { // nested if statement, if double rand divided by double RAND_MAX is less than takeoffProb, inner code will run
            
            takeoffQ.push(curTime); // will run if this if statement and the for loop's requirements are met
            
        } // ends if statement
        
        if (timeFree <= curTime) { // if statement, will run if timeFree is less than or equal to curTime
            
            if (!landingQ.empty()) { // nested if statement, if the factorial of landing.empty() is allowed by the if statement to run
                
                if ((curTime - landingQ.front()) > maxTime) { // double nested if statement, if curTime - landingQ.front() is greater than maxTime, the nested if statement will run, all depending on the previous if statments being true and the for loop actually compiling
                    crashes++; // crashes increments by 1
                    landingQ.pop(); // landingQ.pop() function called into the if statement
                } else { // else statement, performs an action contrary to the if statement just in case it is false and not true
                    landingWaits += (curTime - landingQ.front()); //landingWaits is one integer greater than curTime - landingQ.front()
                    landingQ.pop(); // landingQ.pop() called into the else statement
                    timeFree += landingTime; // timeFree is now one integer higher than landingTime
                    numLandings++; // numLandings increments by 1
                } // else statement ends
                
            } // if statement ends
            else if (!takeoffQ.empty()) { // else if, a fail safe statement, will run if none of the above will run as they are false
                
                takeoffWaits += (curTime - takeoffQ.front()); // takeoffWaits reassigns the problem curTime - takeoffQ.front() in order to keep the sequence moving
                takeoffQ.pop(); // pops the top integer
                timeFree += takeoffTime; // timeFree reassigns takeoffTime to keep sequence moving
                numTakeoffs++; // numTakeoffs increments by 1 pushing the stack upwards
                
            }
            
        }
        
    }
    
    while (!landingQ.empty() && (simulationLength - landingQ.front()) > maxTime) { // while loop, while factorial landingQ.empty and simulationLength - landingQ.front are greater than maxTime, meaning the following code will run if this is correct, and will do so until it stops being correct
        crashes++; // crashes increments by 1 up the queue
        landingQ.pop(); // landingQ.pop called into the while loop
    }
    
    cout << "Number of crashes: " << crashes << "\n"; // displays number of crashes with the given variable
    cout << "Number of takeoffs: " << numTakeoffs << " (avg delay " << // displays number of takeoffs
    ((double) takeoffWaits) / ((double) numTakeoffs) << ")\n"; // double takeoffWaits divided by double numTakeoffs action performed
    cout << "Number of landings: " << numLandings << " (avg delay " << // displays the number of landings
    ((double) landingWaits) / ((double) numLandings) << ")\n"; // action of landingWaits divided by numLandings performed
    cout << "Number of planes in takeoff queue: " << takeoffQ.size() << "\n"; // displays number of takeoff Queues
    cout << "Number of planes in landing queue: " << landingQ.size() << "\n"; // displays numner of landing Queues
    
}

#include "Airport.hpp" // includes Airport header file into program
